# deal-lilim
## matrix bots: interactive and telegram bridges
### to configure the project create a `bot_data.toml`

`bot_data.toml` example configuration:
```toml
[bridge]
name = "@user:homeserver"
password = "password"
tg_token = "<tg token string>"
bridges = [
    {
        mx_id = "!yourmatrixroom:homeserver",
        tg_id = <telegram chat id (i64)>
    },
]

[alembic_bridge]
name = "@user:homeserver"
password = "password"
tg_token = "<tg token string>"
bridges = [
    {
        mx_id = "!yourmatrixroom:homeserver",
        tg_id = <telegram chat id (i64)>
    },
]

[interactive]
name = "@user:homeserver"
password = "password"
room_id = "!yourmatrixroom:homeserver"
anilist_ids = [ <array of anilist user ids (usize)> ]
```
