use crate::bridge_utils::get_tg_bot;
use std::sync::Arc;

use crate::bridge_structs::Bridge;
use crate::tg_handlers::tg_to_mx;
use matrix_sdk::Client;

use teloxide::dispatching::{Dispatcher, UpdateFilterExt};

pub mod bridge_structs;
pub mod bridge_utils;
pub mod db;
pub mod matrix_handlers;
pub mod tg_handlers;

pub async fn dispatch(client: Client, bridges: Arc<Vec<Bridge>>, tg_token: String) {
	let bot = get_tg_bot(tg_token).await;

	let tg_update_handler = teloxide::types::Update::filter_message().endpoint(tg_to_mx);
	Dispatcher::builder(bot, tg_update_handler)
		.dependencies(teloxide::dptree::deps![client, bridges])
		.build()
		.dispatch()
		.await;
}
