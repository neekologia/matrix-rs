use std::sync::Arc;
use std::time::Duration;

use anyhow::Context;
use interactive::commands::{match_command, match_text};
use matrix_sdk::config::SyncSettings;
use matrix_sdk::ruma::events::room::member::StrippedRoomMemberEvent;
use matrix_sdk::ruma::events::room::message::{MessageType, SyncRoomMessageEvent};
use matrix_sdk::ruma::events::SyncMessageLikeEvent;
use matrix_sdk::ruma::RoomId;
use tg_matrix_bridge::bridge_structs::Bridge;
use tg_matrix_bridge::matrix_handlers::client_event_handler;
use toml::Table;

use tracing::{debug, error};

#[derive(Debug, serde::Deserialize)]
pub struct MxBot {
	pub bridge: Table,
	pub alembic_bridge: Table,
	pub interactive: Interactive,
}

#[derive(Debug, serde::Deserialize)]
pub struct Interactive {
	pub name: String,
	pub password: String,
	pub room_id: String,
	pub anilist_ids: Vec<u64>,
}

fn sync_result_handler(res: Result<(), matrix_sdk::Error>) {
	match res {
		Ok(()) => (),
		Err(e) => {
			if let matrix_sdk::Error::Http(matrix_sdk::HttpError::Reqwest(e)) = e {
				if !e.is_timeout() {
					debug!("{:?}", e);
				}
			} else {
				debug!("{:?}", e);
			}
		}
	}
}

pub async fn start_interactive_client(
	store_path: &str,
	device_id_path: &str,
) -> anyhow::Result<()> {
	let mxbot: MxBot = toml::from_str(&std::fs::read_to_string("bot_data.toml")?)?;
	let u = matrix_sdk::ruma::UserId::parse(&mxbot.interactive.name)?;
	let interactive_client = matrix_sdk::Client::builder()
		.sqlite_store(store_path, None)
		.server_name(u.server_name())
		.build()
		.await?;
	let login_builder =
		interactive_client.matrix_auth().login_username(u, &mxbot.interactive.password);
	utils::matrix::read_or_create_device_id(device_id_path, login_builder).await?;
	interactive_client.sync_once(SyncSettings::default()).await?;

	let mut join_set = tokio::task::JoinSet::new();
	let interactive_client_c = interactive_client.clone();

	let room_id = RoomId::parse(mxbot.interactive.room_id)?;
	let utils_client_room =
		Arc::new(interactive_client.get_room(&room_id).context("room not found")?);
	let utils_room = utils_client_room.clone();

	join_set.spawn(async move {
		interactive_client_c.add_event_handler(
			move |ev: SyncRoomMessageEvent, room: matrix_sdk::Room, client: matrix_sdk::Client| async move {
				if ev.sender().as_str() == client.user_id().unwrap().as_str() {
					return;
				}
				if let SyncMessageLikeEvent::Original(original_message) = ev {
					if let (MessageType::Text(text), room) =
						(original_message.content.msgtype.clone(), room.clone())
					{
						let _ = match_command(&room, &text, &original_message).await;
						let _ = match_text(&room, &text, &original_message).await;
					};
				}
			},
		);

		// auto join
		interactive_client_c.add_event_handler(
			|ev: StrippedRoomMemberEvent, room: matrix_sdk::Room, client: matrix_sdk::Client| async move {
				if ev.state_key != client.user_id().unwrap() {
					return;
				}
				if let Err(err) = room.join().await {
					dbg!("{}", err);
				};
			},
		);
	});

	join_set.spawn(async move {
		loop {
			utils::anilist::check(utils_room.clone(), mxbot.interactive.anilist_ids.clone()).await;
		}
	});

	let utils_room = utils_client_room.clone();
	join_set.spawn(async move {
		loop {
			let res = utils::socket_listeners::socket_handler(utils_room.clone()).await;
			error!("{:?}", res);
		}
	});

	loop {
		let res =
			interactive_client.sync(SyncSettings::default().timeout(Duration::from_secs(10))).await;
		sync_result_handler(res);
	}
}

pub async fn start_bridge_client(
	bot_table: &str,
	store_path: &str,
	device_id_path: &str,
) -> anyhow::Result<()> {
	let mxbot: toml::Table = toml::from_str(&std::fs::read_to_string("bot_data.toml")?)?;
	let u = matrix_sdk::ruma::UserId::parse(mxbot[bot_table]["name"].as_str().unwrap())?;
	let bridge_client = matrix_sdk::Client::builder()
		.sqlite_store(store_path, None)
		.server_name(u.server_name())
		.build()
		.await?;
	let login_builder = bridge_client
		.matrix_auth()
		.login_username(u, mxbot[bot_table]["password"].as_str().unwrap());
	utils::matrix::read_or_create_device_id(device_id_path, login_builder).await?;
	bridge_client.sync_once(SyncSettings::default()).await?;

	let mut bridges = vec![];
	for bridge in mxbot[bot_table]["bridges"].as_array().unwrap() {
		bridges.push(Bridge {
			mx_id: bridge["mx_id"].as_str().unwrap().to_string(),
			tg_id: bridge["tg_id"].as_integer().unwrap(),
		});
	}
	let tg_token = mxbot[bot_table]["tg_token"].as_str().unwrap().to_string();

	let bridges = Arc::new(bridges);

	let bridge_client_dispatch = bridge_client.clone();
	let bridge_client_mx_handler = bridge_client.clone();

	let mut join_set = tokio::task::JoinSet::new();
	join_set.spawn(tg_matrix_bridge::dispatch(
		bridge_client_dispatch,
		bridges.clone(),
		tg_token.clone(),
	));

	join_set.spawn(async move {
		bridge_client_mx_handler.add_event_handler(|ev, raw_event, room, client| {
			client_event_handler(ev, raw_event, room, client, bridges, tg_token)
		});
	});

	loop {
		let res =
			bridge_client.sync(SyncSettings::default().timeout(Duration::from_secs(10))).await;
		sync_result_handler(res);
	}
}
