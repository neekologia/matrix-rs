use tracing::info;

use mimalloc::MiMalloc;

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	use tracing_subscriber::{fmt, prelude::*, EnvFilter};
	tracing_subscriber::registry()
		.with(fmt::layer().with_file(true).with_line_number(true))
		.with(EnvFilter::from_default_env())
		.init();

	let mut join_set = tokio::task::JoinSet::new();
	join_set.spawn(client_builders::start_bridge_client(
		"bridge",
		"bridge_store",
		"bridge_device_id",
	));
	join_set.spawn(client_builders::start_bridge_client(
		"alembic_bridge",
		"alembic_store",
		"alembic_bridge_device_id",
	));
	join_set.spawn(client_builders::start_interactive_client(
		"interactive_store",
		"interactive_device_id",
	));
	info!("starting bots");
	join_set.join_all().await;
	Ok(())
}
